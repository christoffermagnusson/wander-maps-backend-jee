package com.magnusson.dev;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("services")
public class App extends ResourceConfig{
	public App(){
		packages("com.magnusson.dev.services");
		this.register(com.magnusson.dev.filters.CrossDomainFilter.class);
	}
}
