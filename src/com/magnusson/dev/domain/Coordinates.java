package com.magnusson.dev.domain;

import java.math.BigDecimal;
import java.math.MathContext;

public final class Coordinates {

	public enum Attribute {
		LNG("lng"), LAT("lat");

		private final String value;

		private Attribute(final String value) {
			this.value = value;
		}

		public String value() {
			return this.value;
		}
	}

	private final double lng;
	private final double lat;

	private Coordinates(final double lng, final double lat) {
		this.lng = lng;
		this.lat = lat;
	}

	/**
	 * Static factory creation method.
	 * 
	 * @param lng
	 * @param lat
	 * @return
	 */
	public static Coordinates of(double lng, double lat) {
		lng = format(lng);
		lat = format(lat);
		return new Coordinates(lng, lat);
	}

	public double lng() {
		return this.lng;
	}

	public double lat() {
		return this.lat;
	}

	@Override
	public String toString() {
		return String.format("Longitude : %f\nLatitude : %f", lng, lat);
	}

	private static double format(double val) {
		int decimals = formatOptions(val);
		BigDecimal formatted = new BigDecimal(val, new MathContext(decimals)); // This
																				// ensures
																				// 7
																				// digits
																				// decimals
																				// as
																				// used
																				// by
																				// maps.
		return formatted.doubleValue();
	}

	/**
	 * Returns the amount of digits to represent the coordinate value. There can
	 * only be a maximum of 7 decimals in the coordinates values, this method
	 * checks if there is 1,2 or 3 digits prior to the decimal point to decide
	 * how many digits the whole number should represented as by the most.
	 * 
	 * For example: 154 has 3 digits and therefore the maximum amount of digits
	 * should be 10, ergo 3 digits prior to the decimals and 7 past it.
	 * 
	 * @param val
	 * @return
	 */
	private static int formatOptions(double val) {
		return val < 10 ? 8 : (val < 100) ? 9 : 10;
	}

}
