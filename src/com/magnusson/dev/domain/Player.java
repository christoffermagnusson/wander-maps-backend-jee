package com.magnusson.dev.domain;

public class Player {
	
	public static enum Attribute{
		NAME("name"),
		HEALTH("health"),
		CLASSNAME("className");
		
		private String value;
		
		private Attribute(String value){
			this.value=value;
		}
		
		public String value(){
			return this.value;
		}
	}
	
	private final String name;
	private final int health;
	private final String className;
	
	private Player(Player.Builder builder){
		this.name		= builder.name;
		this.health		= builder.health;
		this.className	= builder.className;
	}
	
	
	public static class Builder{
		
		private final String name;
		private final int health;
		private final String className;
		
		public Builder(String name, int health, String className){
			this.name		= name;
			this.health		= health;
			this.className	= className;
		}
		
		public Player build(){
			return new Player(this);
		}
	}
	
	// behaviour
	public String name(){
		return this.name;
	}
	
	public int health(){
		return this.health;
	}
	
	public String className(){
		return this.className;
	}
	
	@Override
	public String toString(){
		return String.format("Name : %s\nHealth : %d\nClass : %s"
				,this.name
				,this.health
				,this.className);
	}
	
	

}
