package com.magnusson.dev.domain;

public class Bounds {

	private final double lowerLat;
	private final double upperLat;
	private final double lowerLng;
	private final double upperLng;
	
	private Bounds(Bounds.Builder builder){
		this.lowerLat = builder.lowerLat;
		this.upperLat = builder.upperLat;
		this.lowerLng = builder.lowerLng;
		this.upperLng = builder.upperLng;
	}
	
	
	public static class Builder{
		
		private double lowerLat = -90.0;
		private double upperLat = 90.0;
		private double lowerLng = -180.0;
		private double upperLng = 180.0;
		
		public Builder lowerLat(final double lowerLat){
			this.lowerLat = lowerLat;
			return this;
		}
		
		public Builder upperLat(final double upperLat){
			this.upperLat = upperLat;
			return this;
		}
		
		public Builder lowerLng(final double lowerLng){
			this.lowerLng = lowerLng;
			return this;
		}
		
		public Builder upperLng(final double upperLng){
			this.upperLng = upperLng;
			return this;
		}
		
		public Bounds build(){
			return new Bounds(this);
		}
	}
	
	public double lowerLat(){
		return this.lowerLat;
	}
	
	public double upperLat(){
		return this.upperLat;
	}
	
	public double lowerLng(){
		return this.lowerLng;
	}
	
	public double upperLng(){
		return this.upperLng;
	}
	
	@Override
	public String toString(){
		return String.format("lowerLat %f\nupperLat %f\nlowerLng %f\nupperLng %f",
				this.lowerLat,
				this.upperLat,
				this.lowerLng,
				this.upperLng);
	}
	
}
