package com.magnusson.dev.domain;

public class Monster {
	
	
	// Attributes used for formatting
	public enum Attribute {
		NAME("name"),
		HEALTH("health"),
		ATTACK_POWER("attackPower");
		
		private final String value;
		
		private Attribute(final String value){
			this.value = value;
		}
		
		public String value(){
			return this.value;
		}
	}
	
	
	private final String name;
	private final int health;
	private final int attackPower;
	
	private Monster(Monster.Builder builder){
		this.name 	     = builder.name;
		this.health 	 = builder.health;
		this.attackPower = builder.attackPower;
	}
	
	public static class Builder {
		private final String name;
		private final int health;
		private final int attackPower;
		
		
		public Builder(final String name, final int health, final int attackPower){
			this.name 		 = name;
			this.health 	 = health;
			this.attackPower = attackPower;
		}
		
		public Monster build(){
			return new Monster(this);
		}
	}
	
	// Behaviour
	
	public String name(){
		return this.name;
	}
	
	public int health(){
		return this.health;
	}
	
	public int attackPower(){
		return this.attackPower;
	}
	
	
	@Override
	public String toString(){
		return String.format("Monster name : %s\nMonster health : %d\nMonster attack power : %d",
				this.name,
				this.health,
				this.attackPower);
	}
	
	
	

}
