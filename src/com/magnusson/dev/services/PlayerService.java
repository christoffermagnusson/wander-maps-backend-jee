package com.magnusson.dev.services;

import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;

import org.json.JSONException;
import org.json.JSONObject;

import com.magnusson.dev.domain.Player;
import com.magnusson.dev.formatters.FormatterFactory;
import com.magnusson.dev.formatters.FormatterFactory.Format;
import com.magnusson.dev.formatters.FormattingContext;
import com.magnusson.dev.formatters.FormattingStrategy;
import com.magnusson.dev.storage.PlayerRepository;
import com.magnusson.dev.storage.Repository;

import static com.magnusson.dev.domain.Player.Attribute;

@Path("player")
public class PlayerService {
	
	FormattingContext<Player, String> formattingContext;
	
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{name}")
	public Response getUserByName(@PathParam("name") String name) throws JSONException {
		Repository<String, Player, PlayerRepository.Option> repo = PlayerRepository.getInstance();
		Player player = repo.getByKey(name);
		Response response = null;
		if(player==null){
			response = Response.status(404).build();
		}else{
			FormattingStrategy<Player, String> formattingStrategy = FormatterFactory.playerFormatterOf(Format.JSON); 
			formattingContext = new FormattingContext<>(formattingStrategy);
			String jsonFormattedPlayer = formattingContext.executeFormatting(player);
			response = Response.status(200).entity(jsonFormattedPlayer).build();
		}
		
		return response;
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addUser(String newUser){
		String response = "";
		try{
			JSONObject newUserObj = new JSONObject(newUser);
			checkAttributes(newUserObj.keySet());
			Repository<String,Player, PlayerRepository.Option> repo = PlayerRepository.getInstance();
			Player newPlayer = buildPlayer(newUserObj);
			repo.put(newPlayer.name(), newPlayer);
			response = String.format("Received %s",newUserObj.toString());
		}catch(JSONException je){
			response = String.format("Invalid json. Exception message : %s",je.getMessage());
		}catch(IllegalArgumentException iae){
			response = String.format("Invalid json. Exception message : %s",iae.getMessage());
		}
		return response;
	}
	
	
	// Do checks for values on the different attributes as well.
	private boolean checkAttributes(Set<String> attributes) throws IllegalArgumentException{
		Predicate<String> correctEntries = e -> (e.equals(Attribute.NAME.value()) || e.equals(Attribute.HEALTH.value()) || e.equals(Attribute.CLASSNAME.value()));
		Predicate<String> wrongEntries = e -> !(e.equals(Attribute.NAME.value()) || e.equals(Attribute.HEALTH.value()) || e.equals(Attribute.CLASSNAME.value()));
		boolean correctAttributes = attributes.stream().allMatch(correctEntries);
		if(!correctAttributes){
			Set<String> wrongAttributes = attributes.stream().filter(wrongEntries).collect(Collectors.toSet());
			StringBuilder msg = new StringBuilder("Wrong arguments supplied to POST method: \nCheck names:\n ");
			wrongAttributes.forEach(e -> msg.append(String.format("%s\n",e)));
			throw new IllegalArgumentException(msg.toString());
		}
		return correctAttributes;
	}
	
	private Player buildPlayer(final JSONObject jsonObj){
		String name = jsonObj.getString(Attribute.NAME.value());
		int health = jsonObj.getInt(Attribute.HEALTH.value());
		String className = jsonObj.getString(Attribute.CLASSNAME.value());
		Player player = new Player.Builder(name,health,className).build();
		return player;
	}
	
	
}
