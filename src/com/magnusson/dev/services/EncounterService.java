package com.magnusson.dev.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magnusson.dev.domain.Bounds;
import com.magnusson.dev.domain.Coordinates;
import com.magnusson.dev.domain.Monster;
import com.magnusson.dev.formatters.FormatterFactory;
import com.magnusson.dev.formatters.FormatterFactory.Format;
import com.magnusson.dev.formatters.FormattingContext;
import com.magnusson.dev.formatters.FormattingStrategy;
import com.magnusson.dev.storage.Repository;
import com.magnusson.dev.storage.MonsterRepository;
import com.magnusson.dev.storage.CoordinatesRepository;
import com.magnusson.dev.storage.util.Options;

@Path("encounter")
public class EncounterService {
	
	private FormattingContext<Coordinates, JSONObject> coordToJSONObjCtx;
	private FormattingContext<Monster, JSONObject> monsterToJSONObjCtx;
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEncounters() throws JSONException {
		Response response;
		
		Repository<String, Monster, MonsterRepository.Option> monsterRepo = MonsterRepository.getInstance();
		Repository<Integer, Coordinates, CoordinatesRepository.Option> coordinatesRepo = CoordinatesRepository.getInstance();
		
		List<Monster> availableMonsters = monsterRepo.getAll();
		List<Coordinates> coordinates = coordinatesRepo.getAll();
		
		if(availableMonsters == null || coordinates == null){
			response = Response.status(404).build();
			return response;
		}
		
		JSONArray encounters = buildRandomizedEncounters(coordinates, availableMonsters);
		response = Response.status(200).entity(encounters.toString()).build(); 
		return response;
	}
	
	

	
	/**
	 * Delivers randomized encounters based on user input from client
	 * @param bounds
	 * @return
	 * @throws JSONException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("bounds")
	public Response getAllEncountersBounded(@QueryParam("lats") String lats,
			@QueryParam("lngs") String lngs) throws JSONException {
		Response response = null;
		try{
			// parse lats and lngs as upper and lower bounds
			double [] parsedLats = parseBounds(lats);
			double [] parsedLngs = parseBounds(lngs);
			// create Options object containing a Bounds object
			Bounds bounds = new Bounds.Builder().lowerLat(parsedLats[0])
												.upperLat(parsedLats[1])
												.lowerLng(parsedLngs[0])
												.upperLng(parsedLngs[1])
												.build();
			Options<Bounds> options = Options.withValue(bounds);
			// initiate call to repo with the created Options object
			Repository<Integer, Coordinates, CoordinatesRepository.Option> coordinatesRepo = CoordinatesRepository.getInstance();
			Map<CoordinatesRepository.Option, Options> optionsMap = new HashMap<>();
			optionsMap.put(CoordinatesRepository.Option.BOUNDS, options);
			List<Coordinates> boundedCoordinates = coordinatesRepo.getWithOptions(optionsMap);
			// fetch monsters
			Repository<String, Monster, MonsterRepository.Option> monsterRepo = MonsterRepository.getInstance();
			List<Monster> availableMonsters = monsterRepo.getAll();
			// create encounters within bounds
			JSONArray boundedEncounters = buildRandomizedEncounters(boundedCoordinates, availableMonsters);
			// build response
			response = Response.status(200).entity(boundedEncounters.toString()).build();
		}catch(NumberFormatException nfe){
			System.out.println(nfe.getMessage());
			response = Response.status(404).build();
		}
		
		return response;
	}
	
	/**
	 * Parses inputted lower and upper bounds from clients.
	 * Produces a single double array corresponding of 
	 * lower and upper bounds of either latitude or longitude
	 * @param bounds colon separated String such as "10.000:90.000"
	 * @return
	 * @throws NumberFormatException
	 */
	public double [] parseBounds(String bounds) throws NumberFormatException {
		String [] splitted = bounds.split(":");
		double [] parsedBounds = new double [2];
		double lower = Double.parseDouble(splitted[0]);
		double upper = Double.parseDouble(splitted[1]);
		parsedBounds[0] = lower;
		parsedBounds[1] = upper;
		
		return parsedBounds;
	}
	
	
	private JSONArray buildRandomizedEncounters(List<Coordinates> coordinates, List<Monster> availableMonsters){

		int [] randomPicks = ThreadLocalRandom.current().ints(coordinates.size(), 0, 2).toArray();
		
		// Extract
		// Random encounters
		FormattingStrategy<Coordinates, JSONObject> coordToJSONObjStrategy = FormatterFactory.coordinatesFormatterToJSONObj();
		coordToJSONObjCtx = new FormattingContext<>(coordToJSONObjStrategy);
		
		FormattingStrategy<Monster, JSONObject> monsterToJSONObjStrategy = FormatterFactory.monsterFormatterToJSONObj();
		monsterToJSONObjCtx = new FormattingContext<>(monsterToJSONObjStrategy);
		
		JSONArray encounters = new JSONArray();
		for(int i=0; i<coordinates.size(); i++){
			JSONObject encounterObj  = new JSONObject();
			JSONObject coordinateObj = coordToJSONObjCtx.executeFormatting(coordinates.get(i));
			JSONObject monsterObj 	 = monsterToJSONObjCtx.executeFormatting(availableMonsters.get(randomPicks[i]));
			
			encounterObj.put("coordinates", coordinateObj);
			encounterObj.put("monster", monsterObj);
			
			encounters.put(encounterObj);
		}
		return encounters;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("single")
	public Response getSingleEncounter() throws JSONException {
		Response response;
		
		Repository<Integer, Coordinates, CoordinatesRepository.Option> coordinatesRepository = CoordinatesRepository.getInstance();
		Repository<String, Monster, MonsterRepository.Option> monsterRepository = MonsterRepository.getInstance();
		
		FormattingStrategy<Coordinates, JSONObject> coordToJSONObjStrategy = FormatterFactory.coordinatesFormatterToJSONObj();
		coordToJSONObjCtx = new FormattingContext<>(coordToJSONObjStrategy);
		FormattingStrategy<Monster, JSONObject> monsterToJSONObjStrategy = FormatterFactory.monsterFormatterToJSONObj();
		monsterToJSONObjCtx = new FormattingContext<>(monsterToJSONObjStrategy);
		
		
		JSONObject encounterObject = new JSONObject();
		JSONObject coordObject = coordToJSONObjCtx.executeFormatting(coordinatesRepository.getByKey(0));
		JSONObject monObject = monsterToJSONObjCtx.executeFormatting(monsterRepository.getByKey("Troll"));
		encounterObject.put("coordinates", coordObject);
		encounterObject.put("monster", monObject);
		
		response = Response.status(200).entity(encounterObject.toString()).build();
		return response;
	}

}
