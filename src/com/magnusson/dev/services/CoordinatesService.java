package com.magnusson.dev.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.magnusson.dev.domain.Coordinates;
import com.magnusson.dev.formatters.FormatterFactory;
import com.magnusson.dev.formatters.FormatterFactory.Format;
import com.magnusson.dev.formatters.FormattingContext;
import com.magnusson.dev.formatters.FormattingStrategy;
import com.magnusson.dev.storage.CoordinatesRepository;
import com.magnusson.dev.storage.Repository;

import static com.magnusson.dev.domain.Coordinates.Attribute;


@Path("coordinates")
public class CoordinatesService {

	FormattingContext<List<Coordinates>, String> allCoordsFormattingCtx;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCoordinates() throws JSONException {
		Repository<Integer,Coordinates, CoordinatesRepository.Option> repo = CoordinatesRepository.getInstance();
		List<Coordinates> allCoordinates = repo.getAll();
		Response response = null;
		if(allCoordinates==null){
			response = Response.status(404).build();
			return response;
		}
		FormattingStrategy<List<Coordinates>, String> formattingStrategy = FormatterFactory.coordinatesFormatterOf(Format.JSON);
		allCoordsFormattingCtx = new FormattingContext<>(formattingStrategy);
		String jsonFormattedCoords = allCoordsFormattingCtx.executeFormatting(allCoordinates);
		
		response = Response.status(200).entity(jsonFormattedCoords).build();
		return response;
			
	}
}
