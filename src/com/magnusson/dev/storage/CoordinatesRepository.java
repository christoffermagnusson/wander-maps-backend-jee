package com.magnusson.dev.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import com.magnusson.dev.domain.Bounds;
import com.magnusson.dev.domain.Coordinates;
import com.magnusson.dev.storage.util.Options;

public class CoordinatesRepository implements Repository<Integer, Coordinates, CoordinatesRepository.Option> {

	public enum Option{
		BOUNDS
	}
	
	private static final int LIMIT = 10;
	
	private static CoordinatesRepository INSTANCE = null;
	
	private CoordinatesRepository(){}
	
	public static CoordinatesRepository getInstance(){
		if(INSTANCE == null){
			INSTANCE = new CoordinatesRepository();
		}
		return INSTANCE;
	}
	
	
	Map<Integer, Coordinates> repo = init();
	
	
	@Override
	public Map<Integer, Coordinates> init() {
		return randomizedCoordinates();
	}

	private Map<Integer, Coordinates> randomizedCoordinates() {
		double [] lngs = ThreadLocalRandom.current().doubles(LIMIT, -180.0, 180.0).toArray();
		double [] lats = ThreadLocalRandom.current().doubles(LIMIT, -90.0, 90.0).toArray();
		Map<Integer, Coordinates> coordinates = new HashMap<>();
		for(int i=0; i<lngs.length; i++){
			coordinates.put(i, Coordinates.of(lngs[i], lats[i]));
		}
		return coordinates;
		
	}

	@Override
	public Coordinates getByKey(Integer key) {
		return repo.get(key);
	}

	@Override
	public List<Coordinates> getAll() {
		repo = init(); // reinitializes randomization
		return new ArrayList(repo.values());
	}

	@Override
	public void put(Integer key, Coordinates value) {
		repo.put(key, value);
	}

	@Override
	public List<Coordinates> getWithOptions(Map<CoordinatesRepository.Option, Options> options) {
		if(options.containsKey(Option.BOUNDS)){
			repo = boundedRandomizedCoordinates((Bounds) options.get(Option.BOUNDS).value());
		}
		return new ArrayList(repo.values());
	}
	
	private Map<Integer, Coordinates> boundedRandomizedCoordinates(Bounds bounds){
		double [] lngs = ThreadLocalRandom.current().doubles(LIMIT, bounds.lowerLng(), bounds.upperLng()).toArray();
		double [] lats = ThreadLocalRandom.current().doubles(LIMIT, bounds.lowerLat(), bounds.upperLat()).toArray();
		Map<Integer, Coordinates> coordinates = new HashMap<>();
		for(int i=0; i<lngs.length; i++){
			coordinates.put(i, Coordinates.of(lngs[i], lats[i]));
		}
		return coordinates;
	}

}
