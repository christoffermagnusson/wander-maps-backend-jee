package com.magnusson.dev.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.magnusson.dev.domain.Monster;
import com.magnusson.dev.storage.util.Options;

public class MonsterRepository implements Repository<String, Monster, MonsterRepository.Option> {
	
	public enum Option{
		// add if needed
	}
	
	private static MonsterRepository INSTANCE = null;
	
	private MonsterRepository(){}
	
	public static MonsterRepository getInstance(){
		if(INSTANCE == null){
			INSTANCE = new MonsterRepository();
		}
		return INSTANCE;
	}

	Map<String, Monster> monsters = init();
	
	@Override
	public Map<String, Monster> init() {
		
		Monster monster1 = new Monster.Builder("Imp", 50, 40).build();
		Monster monster2 = new Monster.Builder("Vampire", 90, 25).build();
		Monster monster3 = new Monster.Builder("Troll", 100, 100).build();
		
		Map<String, Monster> initializedMonsters = new HashMap<>();
		initializedMonsters.put(monster1.name(), monster1);
		initializedMonsters.put(monster2.name(), monster2);
		initializedMonsters.put(monster3.name(), monster3);
		
		return initializedMonsters;
	}

	@Override
	public Monster getByKey(String key) {
		// Do checks
		return monsters.get(key);
	}

	@Override
	public List<Monster> getAll() {
		return new ArrayList<Monster>(monsters.values());
	}

	@Override
	public void put(String key, Monster value) {
		monsters.put(key, value);
	}


	@Override
	public List<Monster> getWithOptions(Map<MonsterRepository.Option, Options> options) {
		// TODO Auto-generated method stub
		return null;
	}

}
