package com.magnusson.dev.storage;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.magnusson.dev.storage.util.Options;

public interface Repository <K,V,K2>{

	Map<K,V> init();
	
	V getByKey(K key);
	
	List<V> getAll();
	
	void put(K key, V value);
	
	List<V> getWithOptions(Map<K2, Options> options);
}
