package com.magnusson.dev.storage.util;

public class Options<T> {

	private final T value;
	
	private Options(final T value){
		this.value = value;
	}
	
	public static <T> Options<T> withValue(T value){
		return new Options(value);
	}
	
	public T value(){
		return this.value;
	}
	
}
