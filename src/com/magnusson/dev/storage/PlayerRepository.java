package com.magnusson.dev.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.magnusson.dev.domain.Player;
import com.magnusson.dev.storage.util.Options;

public class PlayerRepository implements Repository<String, Player, PlayerRepository.Option>{
	
	public enum Option{
		
	}
	
	private static PlayerRepository INSTANCE = null;
	
	private PlayerRepository(){};
	
	public static PlayerRepository getInstance(){
		if(INSTANCE==null){
			INSTANCE = new PlayerRepository();
		}
		return INSTANCE;
	}
	
	
	
	Map<String,Player> availablePlayers = init();
	
	public Map<String,Player> init(){
		Player player1 = new Player.Builder("player1", 100, "fighter").build();
		Player player2 = new Player.Builder("player2", 75, "wizard").build();
		Player player3 = new Player.Builder("player3", 125, "berserker").build();
		
		Map<String,Player> players = new HashMap<>();
		players.put(player1.name(), player1);
		players.put(player2.name(), player2);
		players.put(player3.name(), player3);
		
		return players;
	}
	
	public Player getByKey(final String key){
		Player player = null;
		if(availablePlayers.containsKey(key)){
			player = availablePlayers.get(key);
		}else{
			System.out.println(String.format("Could not locate %s",key));
			System.out.println(String.format("PlayerRepository contains %s ? %s",key, availablePlayers.containsKey(key) ? "true" : "false"));
			throw new IllegalArgumentException();
		}
		return player;
	}

	@Override
	public void put(String key, Player value) {
		System.out.println(String.format("Added %s to PlayerRepository", key));
		availablePlayers.put(key, value);
		System.out.println(String.format("PlayerRepository contains %s ? %s",key, availablePlayers.containsKey(key) ? "true" : "false"));
	}

	@Override
	public List<Player> getAll() {
		return new ArrayList(availablePlayers.values());
	}


	@Override
	public List<Player> getWithOptions(Map<Option, Options> options) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
