package com.magnusson.dev.formatters;

import org.json.JSONObject;

import com.magnusson.dev.domain.Coordinates;
import static com.magnusson.dev.domain.Coordinates.Attribute;

public class CoordinatesFormatterJSONObj implements FormattingStrategy<Coordinates, JSONObject> {

	@Override
	public JSONObject format(Coordinates coordinates) {
		JSONObject coordObj = new JSONObject();
		coordObj.put(Attribute.LAT.value(), coordinates.lat());
		coordObj.put(Attribute.LNG.value(), coordinates.lng());
		return coordObj;
	}

}
