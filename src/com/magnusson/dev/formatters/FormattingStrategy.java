package com.magnusson.dev.formatters;

/**
 * Strategy pattern. Formats an object of type T into specified
 * format. (based on what is chosen in FormatterFactory)
 * @author christoffer
 *
 * @param <T> type that gets formatted to another format such as JSON. 
 * 
 * Sample usage: 
 * 
 * <pre>
 * 		Player playerObj = new Player();
 * 		FormattingStrategy<Player> playerStrategy = FormatterFactory.playerFormatterOf(FormatterType.JSON);
 * 		FormattingContext<Player> ctx 			  = new FormattingContext<>(playerStrategy);
 * 		String jsonFormattedPlayer 				  = ctx.executeFormatting(playerObj);
 * </pre>
 */
public interface FormattingStrategy<T, R> {
	
	
	R format(T obj);
}
