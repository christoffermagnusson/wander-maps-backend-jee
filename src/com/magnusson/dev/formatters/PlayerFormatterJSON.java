package com.magnusson.dev.formatters;

import org.json.JSONObject;

import com.magnusson.dev.domain.Player;
import static com.magnusson.dev.domain.Player.Attribute;

/**
 * Used to format a plain java Player object to a JSON formatted
 * String. Also enables plain String formatting of an object. 
 * @author christoffer
 *
 */
public class PlayerFormatterJSON implements FormattingStrategy<Player, String> {
	
	

	@Override
	public String format(Player player) {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put(Attribute.NAME.value(), player.name());
		jsonObj.put(Attribute.HEALTH.value(), player.health());
		jsonObj.put(Attribute.CLASSNAME.value(), player.className());
		return jsonObj.toString();
	}

}
