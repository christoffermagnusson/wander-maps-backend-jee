package com.magnusson.dev.formatters;

import org.json.JSONObject;

import com.magnusson.dev.domain.Monster;
import static com.magnusson.dev.domain.Monster.Attribute;

public class MonsterFormatterJSONObj implements FormattingStrategy<Monster, JSONObject> {

	@Override
	public JSONObject format(Monster monster) {
		JSONObject monsterObj = new JSONObject();
		monsterObj.put(Attribute.NAME.value(), monster.name());
		monsterObj.put(Attribute.HEALTH.value(), monster.health());
		monsterObj.put(Attribute.ATTACK_POWER.value(), monster.attackPower());
		return monsterObj;
	}

}
