package com.magnusson.dev.formatters;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.magnusson.dev.domain.Coordinates;
import com.magnusson.dev.domain.Coordinates.Attribute;

public class CoordinatesFormatterJSON implements FormattingStrategy<List<Coordinates>, String> {

	

	@Override
	public String format(List<Coordinates> allCoordinates) {
		JSONArray coordsArray = new JSONArray();
		allCoordinates.forEach(coordinate -> {
			JSONObject coordObj = new JSONObject();
			coordObj.put(Attribute.LNG.value(), coordinate.lng());
			coordObj.put(Attribute.LAT.value(), coordinate.lat());
			coordsArray.put(coordObj);
		});
		return coordsArray.toString();
	}

}
