package com.magnusson.dev.formatters;

import org.json.JSONObject;

import com.magnusson.dev.domain.Monster;
import static com.magnusson.dev.domain.Monster.Attribute;


/**
 * Formatting strategy to serialize POJO to json String
 * @author christoffer
 *
 */
public class MonsterFormatterJSON implements FormattingStrategy<Monster, String> {


	
	@Override
	public String format(Monster monster) {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put(Attribute.NAME.value(), monster.name());
		jsonObj.put(Attribute.HEALTH.value(), monster.health());
		jsonObj.put(Attribute.ATTACK_POWER.value(), monster.attackPower());
		return jsonObj.toString();
	}

}
