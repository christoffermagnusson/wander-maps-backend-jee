package com.magnusson.dev.formatters;

public class FormattingContext<T, R> {
	
	private FormattingStrategy<T, R> strategy;
	
	public FormattingContext(FormattingStrategy<T, R> strategy){
		this.strategy = strategy;
	}
	
	public R executeFormatting(T obj){
		return this.strategy.format(obj);
	}
	
}
