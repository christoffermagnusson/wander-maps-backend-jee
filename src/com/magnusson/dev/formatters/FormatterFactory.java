package com.magnusson.dev.formatters;

import java.util.List;

import org.json.JSONObject;

import com.magnusson.dev.domain.Coordinates;
import com.magnusson.dev.domain.Monster;
import com.magnusson.dev.domain.Player;

public class FormatterFactory {

	
	public enum Format {
		JSON
	}
	
	// Prevent initialization
	private FormatterFactory(){}
	
	
	// Player Formatting strategies
	public static FormattingStrategy<Player, String> playerFormatterOf(Format type){
		FormattingStrategy<Player, String> formatter;
		switch(type){
			case JSON:
				formatter = new PlayerFormatterJSON();
				break;
			default:
				formatter = new PlayerFormatterJSON();
		}
		
		return formatter;
	}
	
	
	
	// Coordinates Formatting strategies
	public static FormattingStrategy<List<Coordinates>, String> coordinatesFormatterOf(Format type){
		FormattingStrategy<List<Coordinates>, String> formatter;
		switch(type){
			case JSON:
				formatter = new CoordinatesFormatterJSON();
			default:
				formatter = new CoordinatesFormatterJSON();
		}
		return formatter;
	}
	
	public static FormattingStrategy<Coordinates, JSONObject> coordinatesFormatterToJSONObj(){
		FormattingStrategy<Coordinates, JSONObject> formatter = new CoordinatesFormatterJSONObj();
		return formatter;
	}
	
	
	
	// Monster Formatting strategies
	public static FormattingStrategy<Monster, String> monsterFormatterToString(Format type){
		FormattingStrategy<Monster, String> formatter;
		switch(type){
		case JSON:
			formatter = new MonsterFormatterJSON();
		default:
			formatter = new MonsterFormatterJSON();
	}
	return formatter;
	}
	
	
	public static FormattingStrategy<Monster, JSONObject> monsterFormatterToJSONObj(){
		FormattingStrategy<Monster, JSONObject> formatter = new MonsterFormatterJSONObj();
		return formatter;
	}
	
	
}
